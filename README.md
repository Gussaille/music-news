Music News

La page d'accueil affiche des liens vers les dernières news et la liste des derniers albums.
Au clic sur un des liens des news on arrive sur la page de la news en question (/news/:id)
Sur la page de News si l'artiste est enregistré dans le fichier de données, on peut alors visiter la page de l'artiste en question, via "Voir la fiche de l'artiste". (/artists/:id)

La menu de navigation propose 6 onglets: 
- l'Accueil, 
- News (qui énumère toutes les news disponibles), 
- Artistes (qui énumère tous les artistes enregistrés sur l'application), 
- Connexion (/login)
- Dashboard (/dashboard) si l'utilisateur est connecté (amélioration future à afficher si admin déjà connecté)
- Déconnexion (amélioration future à afficher si déjà connecté)

Si l'utilisateur 'admin' se connecte, il a alors accès au dashboard (back-office). (/dashboard) 
Ce back-office affiche des tableaux avec les informations de l'appli et permet d'éditer les news, les artistes, les albums et les concerts. (.../:id/edit)

