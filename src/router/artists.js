import Show from '@/views/artists/Show.vue'
import Edit from '@/views/artists/Edit.vue'
import Index from '@/views/artists/Index.vue'


const routes = [
  {
    path: '/artists',
    name: 'artists.index',
    component: Index,
  },
  {
    path: '/artists/:id/edit',
    name: 'artists.edit',
    component: Edit,
    meta: { auth: true }
  },
  {
    path: '/artists/:id',
    name: 'artists.show',
    component: Show,
  }
]

export default routes
