import Router from 'vue-router'
import Vue from 'vue'
import userApi from '@/api/users'

import Login from "@/views/Login.vue"
import Home from "../views/Home.vue"
import artistsRoutes from '@/router/artists'
import adminRoutes from '@/router/admin'
import concertsRoutes from '@/router/concerts'
import albumsRoutes from '@/router/albums'
import newsRoutes from '@/router/news'
import Main from '@/layouts/Main'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: '/',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      component: Main,
      path: '',
      children: [
        ...newsRoutes,
        ...artistsRoutes,
        ...albumsRoutes,
        ...concertsRoutes,
        ...adminRoutes
      ]
    },
  ]
})

router.beforeEach(async (to, from, next) => {
  if (to.matched.some(route => route.meta.auth)) {
    try {
      await userApi.verifyUser()
      return next()
    } catch (e) {
      localStorage.removeItem('token')
      return next('/login')
    }
  } else {
    return next()
  }
})

export default router
