import Index from '@/views/news/Index.vue'
import Show from '@/views/news/Show.vue'
import Edit from '@/views/news/Edit.vue'

const routes = [
  {
    path: '/news',
    name: 'news.index',
    component: Index,
  },
  {
    path: '/news/:id/edit',
    name: 'news.edit',
    component: Edit,
    meta: { auth: true }
  },
  {
    path: '/news/:id',
    name: 'news.show',
    component: Show,
  }
]

export default routes
