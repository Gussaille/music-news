import Edit from '@/views/concerts/Edit.vue'

const routes = [
  {
    path: '/concerts/:id/edit',
    name: 'concerts.edit',
    component: Edit,
    meta: { auth: true }
  },
]

export default routes
