import Index from '@/views/dashboard/index.vue'

const routes = [
  {
    path: '/dashboard',
    name: 'dashboard.index',
    component: Index,
    meta: { auth: true }
  },
]

export default routes
