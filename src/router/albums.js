import Edit from '@/views/albums/Edit.vue'

const routes = [
  {
    path: '/albums/:id/edit',
    name: 'albums.edit',
    component: Edit,
    meta: { auth: true }
  },
]

export default routes
