import Vuex from 'vuex'
import Vue from 'vue'
import news from '@/store/modules/news'
import artists from '@/store/modules/artists'
import albums from '@/store/modules/albums'
import concerts from '@/store/modules/concerts'
import user from '@/store/modules/user'


Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    news,
    artists,
    albums,
    concerts,
    user,
  }
})
