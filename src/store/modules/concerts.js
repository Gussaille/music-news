import axios from 'axios'

const state = {
  manyConcerts: [],
  concerts: null,
}

const getters = {
  getManyConcerts: state => state.manyConcerts,
  getConcerts: state => state.concerts,
}

const mutations = {
  setManyConcerts (state, manyConcerts) {
    state.manyConcerts = manyConcerts
  },
  setConcerts (state, concerts) {
    state.concerts = concerts
  }
}

const actions = {
  async fetchManyConcerts ({ commit }) {
    const { data } = await axios.get('http://localhost:3000/concerts')
    commit('setManyConcerts', data)
  },
  async fetchConcerts ({ commit }, id) {
    const { data } = await axios.get(`http://localhost:3000/concerts/${id}`)
    commit('setConcerts', data)
  },
  async editConcerts ({ commit }, data) {
    await axios.patch(`http://localhost:3000/concerts/${data.id}`, {
      ...data
    })
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
