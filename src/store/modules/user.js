const state = { token: null }

const mutations = {
  LOGIN_SUCCESS (state, response) {
    state.token = response.data.accessToken
  }
}

const getters = {
  getUserToken: state => state.token,
}

export default {
  state,
  getters,
  mutations
}
