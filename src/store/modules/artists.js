import axios from 'axios'

const state = {
  manyArtists: [],
  artists: null,
}

const getters = {
  getManyArtists: state => state.manyArtists,
  getArtists: state => state.artists,
}

const mutations = {
  setManyArtists (state, manyArtists) {
    state.manyArtists = manyArtists
  },
  setArtists (state, artists) {
    state.artists = artists
  }
}

const actions = {
  async fetchManyArtists ({ commit }) {
    const { data } = await axios.get('http://localhost:3000/artists')
    commit('setManyArtists', data)
  },
  async fetchArtists ({ commit }, id) {
    const { data } = await axios.get(`http://localhost:3000/artists/${id}`)
    commit('setArtists', data)
  },
  async createArtists ({ commit }, data) {
    await axios.post(`http://localhost:3000/artists`, {
      ...data
    })
  },
  async editArtists ({ commit }, data) {
    await axios.patch(`http://localhost:3000/artists/${data.id}`, {
      ...data
    })
  },
  async deleteArtists ({ commit }, id) {
    await axios.delete(`http://localhost:3000/artists/${id}`)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
