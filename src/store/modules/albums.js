import axios from 'axios'

const state = {
  manyAlbums: [],
  albums: null,
}

const getters = {
  getManyAlbums: state => state.manyAlbums,
  getAlbums: state => state.albums,
}

const mutations = {
  setManyAlbums (state, manyAlbums) {
    state.manyAlbums = manyAlbums
  },
  setAlbums (state, albums) {
    state.albums = albums
  }
}

const actions = {
  async fetchManyAlbums ({ commit }) {
    const { data } = await axios.get('http://localhost:3000/albums')
    commit('setManyAlbums', data)
  },
  async fetchAlbums ({ commit }, id) {
    const { data } = await axios.get(`http://localhost:3000/albums/${id}`)
    commit('setAlbums', data)
  },
  async createAlbums ({ commit }, data) {
    await axios.post(`http://localhost:3000/albums`, {
      ...data
    })
  },
  async editAlbums ({ commit }, data) {
    await axios.patch(`http://localhost:3000/albums/${data.id}`, {
      ...data
    })
  },
  async deleteAlbums ({ commit }, id) {
    await axios.delete(`http://localhost:3000/albums/${id}`)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
